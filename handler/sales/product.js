export default {
  pid: 1,
  name: 'Sterling 925 Stud Earrings',
  description: 'A beautiful sterling 925 silver stud earrings. Each earring measures 8mm across and weigh 0.3g',
  images: [
    'https://cdn3.volusion.com/xvdak.oqkom/v/vspfiles/photos/Double-Sided-Silver-2.jpg?1430398981'
  ],
  price: 12.95,
  currency: 'USD',
  paymentMethod: 'COD',
  hasStock: true,
};
