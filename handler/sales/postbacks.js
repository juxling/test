const PostbackPayloads = Object.freeze({
  BUY_NOW: 'buy_now',
  CONFIRM_ORDER: 'confirm_order',
  GET_STARTED_PAYLOAD: 'get_started_payload',
  LEARN_MORE: 'learn_more',
  TALK_TO_SOMEONE: 'talk_to_someone',
});

export default PostbackPayloads;
