import ButtonTemplate from '../../common/components/templates/ButtonTemplate';
import Database from '../../common/models';
import GenericTemplate from '../../common/components/templates/GenericTemplate';
import PostbackButton from '../../common/components/PostBackButton';
import PostbackPayloads from './postbacks';
import ReceiptTemplate from '../../common/components/templates/ReceiptTemplate';
import TextMessage from '../../common/components/messages/TextMessage';
import {QuickReply, QuickReplyTypes} from '../../common/components/QuickReply';

function sendGreetings(user) {
  Database.getDB().Product.findOne({ where: {pid: 1}}) //TODO: get from refparam of C2M ad
    .then((product) => {
      const msg1 = new TextMessage(`Thanks for your interest ${user.firstName}!`);
      const msg2 = new TextMessage(`${product.description}\n\nPrice: ${product.price} ${product.currency}`);

      const msg3 = new GenericTemplate();
      msg3.setTitle(product.name);
      msg3.setImageUrl(product.images[0]);
      msg3.addButton(new PostbackButton('Buy Now', PostbackPayloads.BUY_NOW));
      msg3.addButton(new PostbackButton('Learn More', PostbackPayloads.LEARN_MORE));

      msg1.send(user.psid)
        .then(msg2.send(user.psid))
        .then(msg3.send(user.psid));
    });
}

function sendOrderConfirmation(user) {
  user.getOrders({ where: {oid: user.currentOrderId} }) //oid is a PK
    .then(orders => orders[0])
    .then(order => {
      return Promise.all([
        Database.getDB().Product.findOne({where: {pid: order.pid}}),
        order,
      ]);
    })
    .then(([product, order]) => {
      const msg = new ReceiptTemplate();
      msg.setRecipientName(order.name);
      msg.setOrderNumber(order.oid);
      msg.setCurrency(product.currency);
      msg.setPaymentMethod(product.paymentMethod);
      msg.setTotalCost(product.price * order.quantity);
      msg.setProductTitle(product.name);
      msg.setProductDescription(product.description);
      msg.setProductQuantity(order.quantity);
      msg.setProductImage(product.images[0]);
      return msg.send(user.psid);
    })
    .then(() => {
      const msg = new ButtonTemplate();
      msg.setText('T&C here');
      msg.addButton(new PostbackButton('Confirm Order', PostbackPayloads.CONFIRM_ORDER));
      msg.addButton(new PostbackButton('Make Changes', PostbackPayloads.BUY_NOW));
      msg.send(user.psid);
    });
}

function sendOrderAccepted(user) {
  const msg = new TextMessage("All set! We'll inform you of any updates.");
  msg.send(user.psid);
}

function sendEndDemo(user) {
  const msg = new TextMessage("That's the end of the demo!");
  msg.send(user.psid);
}

function sendResetSuccess(user) {
  const msg = new TextMessage('Say something to get started again :)');
  msg.send(user.psid);
}

function sendMoreInfo(user) {
  const msg = new ButtonTemplate();
  msg.setText('FAQ HERE');
  msg.addButton(new PostbackButton('Buy Now', PostbackPayloads.BUY_NOW));
  msg.addButton(new PostbackButton('Talk to Someone', PostbackPayloads.TALK_TO_SOMEONE));
  msg.send(user.psid);
}

function sendHOPNotice(user) {
  const msg = new TextMessage("Hang on, we'll get back to you shortly!");
  msg.send(user.psid);
}

function askForQuantity(user) {
  const msg = new TextMessage('How many would you like to purchase?');
  for (let i=1; i<4; i++) {
    const qr = new QuickReply(QuickReplyTypes.TEXT);
    qr.setTitle(i);
    qr.setPayload(i);
    msg.addQuickReply(qr);
  }
  msg.send(user.psid);
}

function askForName(user) {
  const msg = new TextMessage('Great! Which name should I put this order under?');
  const qr = new QuickReply(QuickReplyTypes.TEXT);
  qr.setTitle(user.fullName);
  qr.setPayload(user.fullName);
  msg.addQuickReply(qr);
  msg.send(user.psid);
}

function askForPhoneNumber(user) {
  const msg = new TextMessage("I'll need your phone number for shipping");
  const qr = new QuickReply(QuickReplyTypes.PHONE_NUMBER);
  msg.addQuickReply(qr);
  msg.send(user.psid);
}

function askForAddress(user) {
  const msg = new TextMessage('Perfect, now I just need the address to ship it to.');
  msg.send(user.psid);
}

export default {
  askForAddress,
  askForName,
  askForPhoneNumber,
  askForQuantity,
  sendResetSuccess,
  sendEndDemo,
  sendGreetings,
  sendHOPNotice,
  sendMoreInfo,
  sendOrderAccepted,
  sendOrderConfirmation,
};
