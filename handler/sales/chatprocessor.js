import Database from '../../common/models';
import logger from '../../common/logger';

function createOrder(user) {
  Database.getDB().Order.create({ pid: 1}) //TODO: hardcoded
    .then((order) => {
      logger.info(`Created order id: ${order.oid}`);
      return Promise.all([user.addOrder(order), order]);
    })
    .then(([user, order]) => {
      user.update({currentOrderId: order.oid});
    });
}

function processQuantity(user, message) {
  let quantity;
  if (message.quick_reply) {
    quantity = message.quick_reply.payload;
  } else {
    quantity = message.text;
  }
  return user.getOrders({ where: {oid: user.currentOrderId} }) //oid is a PK
    .then((order) => {
      order[0].update({quantity: quantity});
    });
}

function processName(user, message) {
  let name;
  if (message.quick_reply) {
    name = message.quick_reply.payload;
  } else {
    name = message.text;
  }
  return user.getOrders({ where: {oid: user.currentOrderId} }) //oid is a PK
    .then((order) => {
      order[0].update({name: name});
    });
}

function processPhoneNumber(user, message) {
  let number;
  if (message.quick_reply) {
    number = message.quick_reply.payload;
  } else {
    number = message.text;
  }
  return user.getOrders({ where: {oid: user.currentOrderId} }) //oid is a PK
    .then((order) => {
      order[0].update({phoneNumber: number});
    });
}

function processAddress(user, message) {
  const address = message.text;
  return user.getOrders({ where: {oid: user.currentOrderId} }) //oid is a PK
    .then((order) => {
      order[0].update({address: address});
    });
}

export default {
  createOrder,
  processQuantity,
  processName,
  processPhoneNumber,
  processAddress,
};
