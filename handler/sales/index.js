import BaseResponseHandler from '../BaseResponseHandler';
import ChatScript from './chatscript';
import ChatProcessor from './chatprocessor';
import config from '../../common/config';
import Database from '../../common/models';
import PostbackPayloads from './postbacks';
import Product from './product';
import UserStates from './states';
import logger from '../../common/logger';
import hopManager from '../../common/hop';

class SalesResponseHandler extends BaseResponseHandler {
  constructor() {
    super();
    //TODO: optimize by caching product
    return Database.getDB().Product.findOrCreate({
      where: { pid: Product.pid },
      defaults: Product
    });
  }

  messageHandler(user, message) {
    //convenient reset method for testing
    if (message.text === 'reset') {
      ChatScript.sendResetSuccess(user);
      user.update({state: UserStates.NEW_USER});
      return true;
    }

    switch (user.state) {
      case UserStates.NEW_USER:
        ChatScript.sendGreetings(user);
        user.update({state: UserStates.SENT_GREETING});
        break;
      case UserStates.REQUESTED_QUANTITY:
        ChatProcessor.processQuantity(user, message)
          .then(() => {
            ChatScript.askForName(user);
            user.update({state: UserStates.REQUESTED_NAME});
          });
        break;
      case UserStates.REQUESTED_NAME:
        ChatProcessor.processName(user, message)
          .then(() => {
            ChatScript.askForPhoneNumber(user);
            user.update({state: UserStates.REQUESTED_PHONE_NUMBER});
          });
        break;
      case UserStates.REQUESTED_PHONE_NUMBER:
        ChatProcessor.processPhoneNumber(user, message)
          .then(() => {
            ChatScript.askForAddress(user);
            user.update({state: UserStates.REQUESTED_ADDRESS});
          });
        break;
      case UserStates.REQUESTED_ADDRESS:
        ChatProcessor.processAddress(user, message)
          .then(() => {
            ChatScript.sendOrderConfirmation(user);
            user.update({state: UserStates.SENT_ORDER_CONFIRMATION});
          });
        break;
      case UserStates.ORDER_CONFIRMED:
        ChatScript.sendEndDemo(user);
        break;
      default:
        logger.info(`STATE ${user.state} NOT HANDLED!`);
        return false;
    }
    return true;
  }

  postbackHandler(user, postback) {
    //handle postback
    switch (postback.payload) {
      case PostbackPayloads.GET_STARTED_PAYLOAD:
        ChatScript.sendGreetings(user);
        user.update({state: UserStates.SENT_GREETING});
        break;
      case PostbackPayloads.BUY_NOW:
        if (!user.threadControl) {
          hopManager.take_thread_control(user.psid) // triggers handover handler
            .then(() => {
              user.update({threadControl: true});
            });
        } else {
          if (user.currentOrderId === null) {
            ChatProcessor.createOrder(user);
          }
          ChatScript.askForQuantity(user);
          user.update({state: UserStates.REQUESTED_QUANTITY});
        }
        break;
      case PostbackPayloads.LEARN_MORE:
        ChatScript.sendMoreInfo(user);
        user.update({state: UserStates.SENT_MORE_INFO});
        break;
      case PostbackPayloads.TALK_TO_SOMEONE:
        hopManager.pass_thread_control(user.psid)
          .then(() => {
            ChatScript.sendHOPNotice(user);
            user.update({threadControl: false, state: UserStates.SENT_HOP_NOTICE});
          });
        break;
      case PostbackPayloads.CONFIRM_ORDER:
        ChatScript.sendOrderAccepted(user);
        user.update({state: UserStates.ORDER_CONFIRMED});
        break;
      default:
        logger.info(`unhandled postback: ${postback.payload}`);
        return false;
    }
    return true;
  }

  handoverHandler(user, handover) {
    if (
      handover.new_owner_app_id === parseInt(config.fb.APP_ID, 10)
    ) {
      //accept handover and resume bot flow (activate buy now flow)
      ChatProcessor.createOrder(user);
      ChatScript.askForQuantity(user);
      user.update({threadControl: true, state: UserStates.REQUESTED_QUANTITY});
    } else {
      logger.info('unexpected handover received');
      return false;
    }
    return true;
  }
}

export default SalesResponseHandler;
