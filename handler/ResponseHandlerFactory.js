import DefaultResponseHandler from './default';

// Add here new client-specific handlers - see 'default' as ex.
const handlerMap = {
  'default': DefaultResponseHandler
};

class ResponseHandlerFactory {
  constructor(name) {
    return new handlerMap[name]();
  }
}

export default ResponseHandlerFactory;
