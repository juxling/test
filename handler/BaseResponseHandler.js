class BaseResponseHandler {
  messageHandler(user, message) {
    throw new Error('messageHandler() not implemented');
  }

  postbackHandler(user, postback) {
    throw new Error('postbackHandler() not implemented');
  }

  handoverHandler(user, handover) {
    throw new Error('handoverHandler() not implemented');
  }
}

export default BaseResponseHandler;
