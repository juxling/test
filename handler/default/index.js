import BaseResponseHandler from '../BaseResponseHandler';
import constants from '../../common/constants';
import SalesResponseHandler from '../sales';
import config from '../../common/config';
import logger from '../../common/logger';

class DefaultResponseHandler extends BaseResponseHandler {
  constructor() {
    super();
    this.handlers = [];
    const features = config.features;
    features.forEach(feature => {
      switch (feature) {
        case constants.FEATURES_SALES:
          this.handlers.push(new SalesResponseHandler().initialize());
          break;
      }
    });
  }

  messageHandler(user, message) {
    let isHandled = false;
    for (let handler of this.handlers) {
      if (handler.messageHandler(user, message)) {
        isHandled = true;
        break;
      }
    }
    if (!isHandled) {
      // TODO: handle gracefully with a GIVE UP message
      logger.info('unexpected message received');
      this.giveUpHandler();
    }
  }

  postbackHandler(user, postback) {
    let isHandled = false;
    for (let handler of this.handlers) {
      if (handler.postbackHandler(user, postback)) {
        isHandled = true;
        break;
      }
    }
    if (!isHandled) {
      // TODO: handle gracefully with a GIVE UP message
      logger.info('unexpected postback received');
      this.giveUpHandler();
    }
  }

  handoverHandler(user, handover) {
    let isHandled = false;
    for (let handler of this.handlers) {
      if (handler.handoverHandler(user, handover)) {
        isHandled = true;
        break;
      }
    }
    if (!isHandled) {
      logger.info('unexpected handover received');
    }
  }

  giveUpHandler() {
    // TODO implement this to handle errors gracefully
  }
  // TODO add conversational behavior with sectioned text and seen/typing behavior
}

export default DefaultResponseHandler;
