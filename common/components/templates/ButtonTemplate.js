import {BaseTemplate, TemplateTypes} from './BaseTemplate';

// https://developers.facebook.com/docs/messenger-platform/send-messages/template/button
class ButtonTemplate extends BaseTemplate {
  constructor() {
    super();
    this.type = TemplateTypes.BUTTON;
    this.buttons = [];
  }

  setText(text) {
    this.text = text;
  }

  addButton(button) {
    this.buttons.push(button);
  }

  send(psid) {
    const templatePayload = {
      'template_type': this.type,
      'text': this.text,
      'buttons': this.buttons
    };
    super.send(psid, templatePayload);
  }
}

export default ButtonTemplate;
