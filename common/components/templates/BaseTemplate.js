import BaseComponent from '../BaseComponent';

const TemplateTypes = Object.freeze({
  GENERIC: 'generic',
  BUTTON: 'button',
  RECEIPT: 'receipt',
});

class BaseTemplate extends BaseComponent {

  send(psid, templatePayload) {
    const messagePayload = {
      'attachment': {
        'type': 'template',
        'payload': templatePayload
      }
    };
    return super.send(psid, messagePayload);
  }
}

export { BaseTemplate, TemplateTypes };
