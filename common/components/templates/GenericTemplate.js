import {BaseTemplate, TemplateTypes} from './BaseTemplate';

// GenericTemplate supports carousel, but we will omit for now
// https://developers.facebook.com/docs/messenger-platform/reference/template/generic/
class GenericTemplate extends BaseTemplate {
  constructor() {
    super();
    this.type = TemplateTypes.GENERIC;
    this.buttons = [];
  }

  setTitle(title) {
    this.title = title;
  }

  setSubtitle(subtitle) {
    this.subtitle = subtitle;
  }

  setImageUrl(url) {
    this.image_url = url;
  }

  addButton(button) {
    this.buttons.push(button);
  }

  send(psid) {
    const templatePayload = {
      'template_type': this.type,
      'elements':[
        {
          'title': this.title,
          'image_url': this.image_url,
          'subtitle': this.subtitle,
          'buttons':this.buttons
        },
      ]
    };
    super.send(psid, templatePayload);
  }
}

export default GenericTemplate;
