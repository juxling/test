import {BaseTemplate, TemplateTypes} from './BaseTemplate';

// https://developers.facebook.com/docs/messenger-platform/reference/template/receipt/
class ReceiptTemplate extends BaseTemplate {
  constructor() {
    super();
    this.type = TemplateTypes.RECEIPT;
  }

  setRecipientName(name) {
    this.recipientName = name;
  }

  setOrderNumber(number) {
    this.orderNumber = number;
  }

  setCurrency(currency) {
    this.currency = currency;
  }

  setPaymentMethod(paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  setTotalCost(cost) {
    this.totalCost = cost;
  }

  setProductTitle(title) {
    this.productTitle = title;
  }

  setProductDescription(description) {
    this.productDescription = description;
  }

  setProductQuantity(quantity) {
    this.productQuantity = quantity;
  }

  setProductImage(image) {
    this.productImage = image;
  }

  send(psid) {
    const templatePayload = {
      'template_type': this.type,
      'recipient_name': this.recipientName,
      'order_number': this.orderNumber,
      'currency': this.currency,
      'payment_method':this.paymentMethod,
      'summary': {
        'total_cost': this.totalCost
      },
      'elements': [
        {
          'title': this.productTitle,
          'subtitle': this.productDescription,
          'quantity': this.productQuantity,
          'price': this.totalCost,
          'currency': this.currency,
          'image_url': this.productImage,
        }
      ]
    };
    super.send(psid, templatePayload);
  }
}

export default ReceiptTemplate;
