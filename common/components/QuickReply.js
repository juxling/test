const QuickReplyTypes = Object.freeze({
  TEXT: 'text',
  LOCATION: 'location',
  PHONE_NUMBER: 'user_phone_number',
  EMAIL: 'user_email',
});

class QuickReply {
  constructor(contentType) {
    this.contentType = contentType;
  }

  setTitle(title) {
    this.title = title;
  }

  setPayload(payload) {
    this.payload = payload;
  }

  sanitize() {
    return {
      'content_type': this.contentType,
      'title': this.title,
      'payload': this.payload,
    };
  }
}

export {QuickReply, QuickReplyTypes};
