import qm from '../messageQueueHandler';

class BaseComponent {
  send(psid, payload) {
    const messageObj = {
      recipient: {id: psid},
      message: payload,
    };

    return qm.addMessageJob(psid, messageObj);
  }
}

export default BaseComponent;
