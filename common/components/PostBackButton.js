//https://developers.facebook.com/docs/messenger-platform/reference/buttons/postback

class PostbackButton {
  constructor(title, payload) {
    this.type = 'postback';
    this.title = title;
    this.payload = payload;
  }
}

export default PostbackButton;
