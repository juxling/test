import BaseComponent from '../BaseComponent';

class TextMessage extends BaseComponent {
  constructor(text) {
    super();
    this.text = text;
    this.quickReplies = [];
  }

  addQuickReply(quickReply) {
    this.quickReplies.push(quickReply.sanitize());
  }

  send(psid) {
    const messagePayload = {
      'text': this.text,
    };
    if (this.quickReplies.length > 0) {
      messagePayload['quick_replies'] = this.quickReplies;
    }
    return super.send(psid, messagePayload);
  }
}

export default TextMessage;
