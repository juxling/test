import config from './config';
import messagequeue from './messagequeue';
import messagequeueSQLite from './messagequeueSQLite';

let messageQueueHandler;
if (config.db.USE_DB == 'SQLITE') {
  messageQueueHandler = messagequeueSQLite;
} else {
  messageQueueHandler = messagequeue;
}
export default {
  addMessageJob:(psid, messageObj) => {
    return messageQueueHandler.addMessageJob(psid, messageObj);
  }
};
