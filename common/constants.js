const SB_CONSTANTS = {
  GRAPH_BASE_URL: 'https://graph.facebook.com/v2.11',
  PAGES_INBOX_APP_ID: '263902037430900',
  WEBHOOK_VERIFY_TOKEN: 'SALES_BOT',
  FEATURES_SALES: 'sales',
};

export default SB_CONSTANTS;
