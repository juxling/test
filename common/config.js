import constants from './constants';
import logger from './logger';

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

const features = [];
try {
  let features_json = JSON.parse(process.env.USE_FEATURES);
  features_json.forEach(feature => {
    switch (feature) {
      case constants.FEATURES_SALES:
        features.push(constants.FEATURES_SALES);
        break;
    }
  });
} catch (e) {
  logger.error('USE_FEATURES is an invalid JSON string.');
}

if (features.length == 0) {
  logger.error('No valid features found.');
}

const config = Object.freeze({
  db: {
    HOST: process.env.SB_DB_HOST,
    NAME: process.env.SB_DB_NAME,
    USERNAME: process.env.SB_DB_USER_NAME,
    USE_DB: process.env.USE_DB,
    SQLITE_PATH: process.env.SQLITE_PATH
  },
  server: {
    PORT: process.env.SB_SERVER_PORT
  },
  advertiser: {
    NAME: process.env.SB_ADVERTISER_NAME
  },
  fb: {
    APP_ID: process.env.APP_ID,
    PAGE_ACCESS_TOKEN: process.env.PAGE_ACCESS_TOKEN
  },
  features: features
});

module.exports = config;
