import PgBoss from 'pg-boss';
import config from './config';
import constants from './constants';
import fbrequest from './fbrequest';
import logger from './logger';

let db_url;
if (process.env.NODE_ENV === 'production') {
  db_url = process.env.DATABASE_URL;
} else {
  db_url = `postgres://${config.db.USERNAME}@${config.db.HOST}/${config.db.NAME}`;
}

const queueName = 'message-job';
const options = {
  connectionString: db_url,
  //TODO: to make sure for the same destination psid, jobs are processed in order, we need concurrency control
  //setting global pool size to 1, but in the future we need to customize subscribe to only limit to psid
  //https://github.com/timgit/pg-boss/issues/63
  poolSize: 1,
  archiveCompletedJobsEvery: '2 days'
};

// TODO make this into a class
let boss;
if (config.db.USE_DB === 'POSTGRES') {
  boss = new PgBoss(options);
  boss.on('error', onError);
  boss.start()
    .then(ready)
    .catch(onError);
}

function ready() {
  boss.subscribe(queueName, messageJobHandler)
    .then(logger.info(`Subscribed to ${queueName} queue`))
    .catch(onError);

  logger.info(`${queueName} queue ready`);
}

function onError(error) {
  logger.info(`message-job error:${JSON.stringify(error, null, 2)}`);
}

/* worker function */
function messageJobHandler(job) {
  const messageObj = job.data.messageObj;
  logger.info(`Processing job id:${job.id})`);
  logger.info(`Sending message ${JSON.stringify(messageObj)}.`);
  fbrequest.post({
    uri: `${constants.GRAPH_BASE_URL}/me/messages`,
    qs: { access_token: config.fb.PAGE_ACCESS_TOKEN },
    json: messageObj,
  })
    .then(logger.info('Message sent successfully'))
    .catch(err => logger.info(`Message sending failed with error: ${err}`))
    .finally(() => {
      job.done()
        .then(logger.info(`Job ${job.id} completed`))
        .catch(onError);
    });
}

export default {
  addMessageJob:(psid, messageObj) => {
    return boss.publish(queueName, {psid, messageObj})
      .then(jobId => {
        logger.info(`Message \n${JSON.stringify(messageObj, null, 2)} added to job queue, id:${jobId}`);
        return jobId;
      })
      .catch(onError);
  }
};
