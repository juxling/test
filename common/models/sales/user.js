const user = (sequelize, DataTypes) => {
  const User = sequelize.define('user',
    {
      psid: {
        type: DataTypes.STRING,
        primaryKey: true,
        unique: true,
      },
      firstName: {
        type: DataTypes.STRING,
      },
      lastName: {
        type: DataTypes.STRING,
      },
      gender: {
        type: DataTypes.STRING,
      },
      state: {
        type: DataTypes.STRING,
        defaultValue: null,
      },
      currentOrderId: {
        type: DataTypes.INTEGER,
        defaultValue: null,
      },
      threadControl: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      }
    },
    {
      getterMethods: {
        fullName() {
          return this.firstName + ' ' + this.lastName;
        }
      }
    }
  );

  User.associate = models => {
    User.hasMany(models.Order);
  };

  return User;
};

export default user;
