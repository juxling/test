const order = (sequelize, DataTypes) => {
  const Order = sequelize.define('order',
    {
      oid: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      pid: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
      },
      name: {
        type: DataTypes.STRING,
      },
      address: {
        type: DataTypes.STRING,
      },
      phoneNumber: {
        type: DataTypes.STRING
      },
    },
  );

  Order.associate = models => {
    Order.belongsTo(models.User);
  };

  return Order;
};

export default order;
