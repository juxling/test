import config from '../config';
import configModels from '../configModels';
import logger from '../logger';
import Sequelize from 'sequelize';

class Database {
  constructor() {
    this.initdb();
  }

  initdb() {
    let db_url;
    if (process.env.NODE_ENV === 'production') {
      db_url = process.env.DATABASE_URL;
    } else {
      if (config.db.USE_DB == 'SQLITE') {
        db_url = 'database';
      } else {
        db_url = `postgres://${config.db.USERNAME}@${config.db.HOST}/${config.db.NAME}`;
      }
    }

    let dbConfig = {
      operatorsAliases: false,
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      },
      logging: process.env.NODE_DEV !== 'production' ? console.log : false,
    };

    let sequelize;
    if (config.db.USE_DB == 'SQLITE') {
      dbConfig['dialect'] = 'sqlite';
      dbConfig['host'] = '0.0.0.0';
      dbConfig['storage'] = `${config.db.SQLITE_PATH}`;
      sequelize = new Sequelize('database', '', '', dbConfig); // USERNAME, PASS
    } else {
      dbConfig['dialect'] = 'postgres';
      sequelize = new Sequelize(db_url, dbConfig);
    }

    this.db = {};
    sequelize.authenticate()
      .then(() => {
        logger.info('DB connection has been established successfully.');
        config.features.forEach((feature) => {
          for (let component_key in configModels[feature]) {
            logger.info('Adding: ' + component_key);
            this.db[component_key] = sequelize.import(configModels[feature][component_key]);
          }
        });

        Object.keys(this.db).forEach((modelName) => {
          if (this.db[modelName].associate) {
            this.db[modelName].associate(this.db);
          }
        });
        sequelize.sync();
      })
      .catch(err => {
        logger.error('Unable to connect to the database:', err);
      });

    this.db.sequelize = sequelize;
    this.db.Sequelize = Sequelize;
  }

  getDB() {
    if (this.db === null) {
      this.initdb();
    }
    return this.db;
  }
}

module.exports = Database;
