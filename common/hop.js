import config from './config';
import constants from './constants';
import fbrequest from './fbrequest';
import logger from './logger';

function pass_thread_control(psid) {
  const hopPayload = {
    'recipient': {'id': psid},
    'target_app_id': constants.PAGES_INBOX_APP_ID,
    'metadata': 'test metadata from bot'
  };

  const page_access_token = config.fb.PAGE_ACCESS_TOKEN;
  logger.info('Initiating handover to Pages Inbox');

  return fbrequest.post({
    uri: `${constants.GRAPH_BASE_URL}/me/pass_thread_control`,
    qs: { access_token: page_access_token },
    json: hopPayload,
  })
  .then(() => { logger.info('Handover to Pages Inbox success'); })
  .catch((err) => { logger.info(`Handover to Pages Inbox failed: ${JSON.stringify(err)}`); });
}

function take_thread_control(psid) {
  const hopPayload = {
    'recipient': {'id': psid},
  };

  const page_access_token = config.fb.PAGE_ACCESS_TOKEN;
  logger.info('Initiating takeover from Pages Inbox');

  return fbrequest.post({
    uri: `${constants.GRAPH_BASE_URL}/me/take_thread_control`,
    qs: { access_token: page_access_token },
    json: hopPayload,
  })
  .then(() => { logger.info('Takeover from Pages Inbox success'); })
  .catch((err) => { logger.info(`Takeover from Pages Inbox failed: ${JSON.stringify(err)}`); });
}

export default {
  pass_thread_control,
  take_thread_control,
};
