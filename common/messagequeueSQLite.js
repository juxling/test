import Queue from 'node-persistent-queue';
import config from './config';
import constants from './constants';
import fbrequest from './fbrequest';
import logger from './logger';

var q = new Queue(`${config.db.SQLITE_PATH}`, 1);

q.on('open', function() {
  logger.info('Opening SQLite DB');
  logger.info('Queue contains ' + q.getLength() + ' job/s');
});

q.on('add', function(task) {
  logger.info('Adding task: ' + JSON.stringify(task));
  logger.info('Queue contains ' + q.getLength() + ' jobs');
});

q.on('start', function() {
  logger.info('Starting queue');
});

q.on('next', function(task) {
  messageJobHandler(task);
});

q.on('empty', function() {
  logger.info('Queue is empty');
});

q.on('stop', function() {
  logger.info('Stopping queue');
});

q.on('close', function() {
  logger.info('Closing SQLite DB');
});

q.open()
.then(function() {
  q.start();
})
.catch(function(err) {
  onError(err);
});

function onError(error) {
  logger.info(`message-job error:${JSON.stringify(error, null, 2)}`);
}

/* worker function */
function messageJobHandler(job) {
  const messageObj = job.messageObj;
  logger.info(`Sending message ${JSON.stringify(messageObj)}.`);
  fbrequest.post({
    uri: `${constants.GRAPH_BASE_URL}/me/messages`,
    qs: { access_token: config.fb.PAGE_ACCESS_TOKEN },
    json: messageObj,
  })
    .then(logger.info('Message sent successfully'))
    .catch(err => logger.info(`Message sending failed with error: ${err}`))
    .finally(() => {
      q.done();
    });
}

export default {
  addMessageJob:(psid, messageObj) => {
    return q.add({psid, messageObj});
  }
};
