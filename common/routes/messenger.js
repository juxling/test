import config from '../config';
import constants from '../constants';
import db from '../models';
import express from 'express';
import fbrequest from '../fbrequest';
import logger from '../logger';
import ResponseHandlerFactory from '../../handler/ResponseHandlerFactory';

const router = express.Router();

//Messenger Webhook Verification
router.get('/webhook', (req, res) => {
  const mode = req.query['hub.mode'];
  const token = req.query['hub.verify_token'];
  const challenge = req.query['hub.challenge'];

  if (mode && token) {
    if (mode === 'subscribe' && token === constants.WEBHOOK_VERIFY_TOKEN) {
      logger.info('Webhook Verification Complete');
      res.status(200).send(challenge);
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});

//Messenger Webhook Events
router.post('/webhook', (req, res) => {
  res.sendStatus(200); //respond immediately

  const body = req.body;
  if (body.object === 'page') {
    body.entry.forEach((entry) => {
      if (entry.messaging) {
        entry.messaging.forEach((messagingEvent) => {
          logger.info(`incoming event:\n${JSON.stringify(messagingEvent, null, 2)}`);

          db.User.findOrCreate({
            where: {psid: messagingEvent.sender.id}
          })
          .spread((user, created) => {
            if (created) {
              return updateUserProfile(user);
            }
            return user;
          })
          .then((user) => {
            const handler = new ResponseHandlerFactory(config.advertiser.NAME);
            handler.then(() => {
              if (messagingEvent.message) {
                handler.messageHandler(user, messagingEvent.message);
              } else if (messagingEvent.postback) {
                handler.postbackHandler(user, messagingEvent.postback);
              } else if (messagingEvent.pass_thread_control) {
                handler.handoverHandler(user, messagingEvent.pass_thread_control);
              } else {
                logger.info('unexpected event');
              }
            });
          });
        });
      } else if (entry.standby) {
        entry.standby.forEach((messagingEvent) => {
          //do nothing for standby
          logger.info(`GOT STANDBY: ${JSON.stringify(messagingEvent, null, 2)}`);
        });
      } else {
        logger.info('unexpected entry');
      }
    });
  }
});

const updateUserProfile = (user) => {
  logger.info(`Getting user profile for psid: ${user.psid}`);
  return fbrequest.get({
    uri: `${constants.GRAPH_BASE_URL}/${user.psid}`,
    qs: {
      fields: 'first_name, last_name',
      access_token: config.fb.PAGE_ACCESS_TOKEN
    },
  })
  .then((res) => {
    logger.info('Received user profile, updating DB');
    return user.update({
      firstName: res.first_name,
      lastName: res.last_name
    });
  })
  .catch((err) => logger.info(`Could not retreive user profile:\n${JSON.stringify(err, null, 2)}`))
  .then(() => user);
};

export default router;
