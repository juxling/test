import express from 'express';
import messenger from './messenger';

let router = express.Router();

router.use('/messenger', messenger);

export default router;
