#!/bin/sh

if [ "$NODE_ENV" = "production" ]; then
  node bundle/server.js
else
  nodemon --exec babel-node server.js
fi
