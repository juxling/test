#!/bin/sh

rm -rf bundle/*
NODE_ENV=production ./node_modules/.bin/babel server.js --out-file bundle/server.js --source-maps
NODE_ENV=production ./node_modules/.bin/babel common --out-dir bundle/common/ --source-maps
NODE_ENV=production ./node_modules/.bin/babel handler --out-dir bundle/handler/ --source-maps
echo "Build Complete!"
