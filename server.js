import config from './common/config';
import express from 'express';
import bodyParser from 'body-parser';
import logger from './common/logger';
import router from './common/routes';
import Database from './common/models';

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', router);

// force: false means do not drop tables if exist
const db = new Database();
db.getDB().sequelize.sync({force: false}).then(() => {
  app.listen(process.env.PORT || config.server.PORT, () => {
    logger.info(`Salesbot listening on port ${config.server.PORT} with\nconfig = ${JSON.stringify(config, null, 2)}`);
  });
});

module.exports = db;
