'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _BaseResponseHandler2 = require('../BaseResponseHandler');

var _BaseResponseHandler3 = _interopRequireDefault(_BaseResponseHandler2);

var _chatscript = require('./chatscript');

var _chatscript2 = _interopRequireDefault(_chatscript);

var _chatprocessor = require('./chatprocessor');

var _chatprocessor2 = _interopRequireDefault(_chatprocessor);

var _config = require('../../common/config');

var _config2 = _interopRequireDefault(_config);

var _models = require('../../common/models');

var _models2 = _interopRequireDefault(_models);

var _postbacks = require('./postbacks');

var _postbacks2 = _interopRequireDefault(_postbacks);

var _product = require('./product');

var _product2 = _interopRequireDefault(_product);

var _states = require('./states');

var _states2 = _interopRequireDefault(_states);

var _logger = require('../../common/logger');

var _logger2 = _interopRequireDefault(_logger);

var _hop = require('../../common/hop');

var _hop2 = _interopRequireDefault(_hop);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SalesResponseHandler = function (_BaseResponseHandler) {
  _inherits(SalesResponseHandler, _BaseResponseHandler);

  function SalesResponseHandler() {
    var _ret;

    _classCallCheck(this, SalesResponseHandler);

    //TODO: optimize by caching product
    var _this = _possibleConstructorReturn(this, (SalesResponseHandler.__proto__ || Object.getPrototypeOf(SalesResponseHandler)).call(this));

    return _ret = _models2.default.getDB().Product.findOrCreate({
      where: { pid: _product2.default.pid },
      defaults: _product2.default
    }), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(SalesResponseHandler, [{
    key: 'messageHandler',
    value: function messageHandler(user, message) {
      //convenient reset method for testing
      if (message.text === 'reset') {
        _chatscript2.default.sendResetSuccess(user);
        user.update({ state: _states2.default.NEW_USER });
        return true;
      }

      switch (user.state) {
        case _states2.default.NEW_USER:
          _chatscript2.default.sendGreetings(user);
          user.update({ state: _states2.default.SENT_GREETING });
          break;
        case _states2.default.REQUESTED_QUANTITY:
          _chatprocessor2.default.processQuantity(user, message).then(function () {
            _chatscript2.default.askForName(user);
            user.update({ state: _states2.default.REQUESTED_NAME });
          });
          break;
        case _states2.default.REQUESTED_NAME:
          _chatprocessor2.default.processName(user, message).then(function () {
            _chatscript2.default.askForPhoneNumber(user);
            user.update({ state: _states2.default.REQUESTED_PHONE_NUMBER });
          });
          break;
        case _states2.default.REQUESTED_PHONE_NUMBER:
          _chatprocessor2.default.processPhoneNumber(user, message).then(function () {
            _chatscript2.default.askForAddress(user);
            user.update({ state: _states2.default.REQUESTED_ADDRESS });
          });
          break;
        case _states2.default.REQUESTED_ADDRESS:
          _chatprocessor2.default.processAddress(user, message).then(function () {
            _chatscript2.default.sendOrderConfirmation(user);
            user.update({ state: _states2.default.SENT_ORDER_CONFIRMATION });
          });
          break;
        case _states2.default.ORDER_CONFIRMED:
          _chatscript2.default.sendEndDemo(user);
          break;
        default:
          _logger2.default.info('STATE ' + user.state + ' NOT HANDLED!');
          return false;
      }
      return true;
    }
  }, {
    key: 'postbackHandler',
    value: function postbackHandler(user, postback) {
      //handle postback
      switch (postback.payload) {
        case _postbacks2.default.GET_STARTED_PAYLOAD:
          _chatscript2.default.sendGreetings(user);
          user.update({ state: _states2.default.SENT_GREETING });
          break;
        case _postbacks2.default.BUY_NOW:
          if (!user.threadControl) {
            _hop2.default.take_thread_control(user.psid) // triggers handover handler
            .then(function () {
              user.update({ threadControl: true });
            });
          } else {
            if (user.currentOrderId === null) {
              _chatprocessor2.default.createOrder(user);
            }
            _chatscript2.default.askForQuantity(user);
            user.update({ state: _states2.default.REQUESTED_QUANTITY });
          }
          break;
        case _postbacks2.default.LEARN_MORE:
          _chatscript2.default.sendMoreInfo(user);
          user.update({ state: _states2.default.SENT_MORE_INFO });
          break;
        case _postbacks2.default.TALK_TO_SOMEONE:
          _hop2.default.pass_thread_control(user.psid).then(function () {
            _chatscript2.default.sendHOPNotice(user);
            user.update({ threadControl: false, state: _states2.default.SENT_HOP_NOTICE });
          });
          break;
        case _postbacks2.default.CONFIRM_ORDER:
          _chatscript2.default.sendOrderAccepted(user);
          user.update({ state: _states2.default.ORDER_CONFIRMED });
          break;
        default:
          _logger2.default.info('unhandled postback: ' + postback.payload);
          return false;
      }
      return true;
    }
  }, {
    key: 'handoverHandler',
    value: function handoverHandler(user, handover) {
      if (handover.new_owner_app_id === parseInt(_config2.default.fb.APP_ID, 10)) {
        //accept handover and resume bot flow (activate buy now flow)
        _chatprocessor2.default.createOrder(user);
        _chatscript2.default.askForQuantity(user);
        user.update({ threadControl: true, state: _states2.default.REQUESTED_QUANTITY });
      } else {
        _logger2.default.info('unexpected handover received');
        return false;
      }
      return true;
    }
  }]);

  return SalesResponseHandler;
}(_BaseResponseHandler3.default);

exports.default = SalesResponseHandler;
//# sourceMappingURL=index.js.map