'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var PostbackPayloads = Object.freeze({
  BUY_NOW: 'buy_now',
  CONFIRM_ORDER: 'confirm_order',
  GET_STARTED_PAYLOAD: 'get_started_payload',
  LEARN_MORE: 'learn_more',
  TALK_TO_SOMEONE: 'talk_to_someone'
});

exports.default = PostbackPayloads;
//# sourceMappingURL=postbacks.js.map