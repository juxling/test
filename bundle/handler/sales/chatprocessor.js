'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _models = require('../../common/models');

var _models2 = _interopRequireDefault(_models);

var _logger = require('../../common/logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createOrder(user) {
  _models2.default.getDB().Order.create({ pid: 1 }) //TODO: hardcoded
  .then(function (order) {
    _logger2.default.info('Created order id: ' + order.oid);
    return Promise.all([user.addOrder(order), order]);
  }).then(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        user = _ref2[0],
        order = _ref2[1];

    user.update({ currentOrderId: order.oid });
  });
}

function processQuantity(user, message) {
  var quantity = void 0;
  if (message.quick_reply) {
    quantity = message.quick_reply.payload;
  } else {
    quantity = message.text;
  }
  return user.getOrders({ where: { oid: user.currentOrderId } }) //oid is a PK
  .then(function (order) {
    order[0].update({ quantity: quantity });
  });
}

function processName(user, message) {
  var name = void 0;
  if (message.quick_reply) {
    name = message.quick_reply.payload;
  } else {
    name = message.text;
  }
  return user.getOrders({ where: { oid: user.currentOrderId } }) //oid is a PK
  .then(function (order) {
    order[0].update({ name: name });
  });
}

function processPhoneNumber(user, message) {
  var number = void 0;
  if (message.quick_reply) {
    number = message.quick_reply.payload;
  } else {
    number = message.text;
  }
  return user.getOrders({ where: { oid: user.currentOrderId } }) //oid is a PK
  .then(function (order) {
    order[0].update({ phoneNumber: number });
  });
}

function processAddress(user, message) {
  var address = message.text;
  return user.getOrders({ where: { oid: user.currentOrderId } }) //oid is a PK
  .then(function (order) {
    order[0].update({ address: address });
  });
}

exports.default = {
  createOrder: createOrder,
  processQuantity: processQuantity,
  processName: processName,
  processPhoneNumber: processPhoneNumber,
  processAddress: processAddress
};
//# sourceMappingURL=chatprocessor.js.map