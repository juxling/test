'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var UserStates = Object.freeze({
  NEW_USER: null,
  ORDER_CONFIRMED: 'order_confirmed',
  REQUESTED_QUANTITY: 'requested_quantity',
  REQUESTED_NAME: 'requested_name',
  REQUESTED_PHONE_NUMBER: 'requested_phone_number',
  REQUESTED_ADDRESS: 'requested_address',
  SENT_GREETING: 'sent_greeting',
  SENT_HOP_NOTICE: 'sent_hop_notice',
  SENT_MORE_INFO: 'sent_more_info',
  SENT_ORDER_CONFIRMATION: 'sent_order_confirmation'
});

exports.default = UserStates;
//# sourceMappingURL=states.js.map