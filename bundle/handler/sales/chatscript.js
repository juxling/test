'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _ButtonTemplate = require('../../common/components/templates/ButtonTemplate');

var _ButtonTemplate2 = _interopRequireDefault(_ButtonTemplate);

var _models = require('../../common/models');

var _models2 = _interopRequireDefault(_models);

var _GenericTemplate = require('../../common/components/templates/GenericTemplate');

var _GenericTemplate2 = _interopRequireDefault(_GenericTemplate);

var _PostBackButton = require('../../common/components/PostBackButton');

var _PostBackButton2 = _interopRequireDefault(_PostBackButton);

var _postbacks = require('./postbacks');

var _postbacks2 = _interopRequireDefault(_postbacks);

var _ReceiptTemplate = require('../../common/components/templates/ReceiptTemplate');

var _ReceiptTemplate2 = _interopRequireDefault(_ReceiptTemplate);

var _TextMessage = require('../../common/components/messages/TextMessage');

var _TextMessage2 = _interopRequireDefault(_TextMessage);

var _QuickReply = require('../../common/components/QuickReply');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function sendGreetings(user) {
  _models2.default.getDB().Product.findOne({ where: { pid: 1 } }) //TODO: get from refparam of C2M ad
  .then(function (product) {
    var msg1 = new _TextMessage2.default('Thanks for your interest ' + user.firstName + '!');
    var msg2 = new _TextMessage2.default(product.description + '\n\nPrice: ' + product.price + ' ' + product.currency);

    var msg3 = new _GenericTemplate2.default();
    msg3.setTitle(product.name);
    msg3.setImageUrl(product.images[0]);
    msg3.addButton(new _PostBackButton2.default('Buy Now', _postbacks2.default.BUY_NOW));
    msg3.addButton(new _PostBackButton2.default('Learn More', _postbacks2.default.LEARN_MORE));

    msg1.send(user.psid).then(msg2.send(user.psid)).then(msg3.send(user.psid));
  });
}

function sendOrderConfirmation(user) {
  user.getOrders({ where: { oid: user.currentOrderId } }) //oid is a PK
  .then(function (orders) {
    return orders[0];
  }).then(function (order) {
    return Promise.all([_models2.default.getDB().Product.findOne({ where: { pid: order.pid } }), order]);
  }).then(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        product = _ref2[0],
        order = _ref2[1];

    var msg = new _ReceiptTemplate2.default();
    msg.setRecipientName(order.name);
    msg.setOrderNumber(order.oid);
    msg.setCurrency(product.currency);
    msg.setPaymentMethod(product.paymentMethod);
    msg.setTotalCost(product.price * order.quantity);
    msg.setProductTitle(product.name);
    msg.setProductDescription(product.description);
    msg.setProductQuantity(order.quantity);
    msg.setProductImage(product.images[0]);
    return msg.send(user.psid);
  }).then(function () {
    var msg = new _ButtonTemplate2.default();
    msg.setText('T&C here');
    msg.addButton(new _PostBackButton2.default('Confirm Order', _postbacks2.default.CONFIRM_ORDER));
    msg.addButton(new _PostBackButton2.default('Make Changes', _postbacks2.default.BUY_NOW));
    msg.send(user.psid);
  });
}

function sendOrderAccepted(user) {
  var msg = new _TextMessage2.default("All set! We'll inform you of any updates.");
  msg.send(user.psid);
}

function sendEndDemo(user) {
  var msg = new _TextMessage2.default("That's the end of the demo!");
  msg.send(user.psid);
}

function sendResetSuccess(user) {
  var msg = new _TextMessage2.default('Say something to get started again :)');
  msg.send(user.psid);
}

function sendMoreInfo(user) {
  var msg = new _ButtonTemplate2.default();
  msg.setText('FAQ HERE');
  msg.addButton(new _PostBackButton2.default('Buy Now', _postbacks2.default.BUY_NOW));
  msg.addButton(new _PostBackButton2.default('Talk to Someone', _postbacks2.default.TALK_TO_SOMEONE));
  msg.send(user.psid);
}

function sendHOPNotice(user) {
  var msg = new _TextMessage2.default("Hang on, we'll get back to you shortly!");
  msg.send(user.psid);
}

function askForQuantity(user) {
  var msg = new _TextMessage2.default('How many would you like to purchase?');
  for (var i = 1; i < 4; i++) {
    var qr = new _QuickReply.QuickReply(_QuickReply.QuickReplyTypes.TEXT);
    qr.setTitle(i);
    qr.setPayload(i);
    msg.addQuickReply(qr);
  }
  msg.send(user.psid);
}

function askForName(user) {
  var msg = new _TextMessage2.default('Great! Which name should I put this order under?');
  var qr = new _QuickReply.QuickReply(_QuickReply.QuickReplyTypes.TEXT);
  qr.setTitle(user.fullName);
  qr.setPayload(user.fullName);
  msg.addQuickReply(qr);
  msg.send(user.psid);
}

function askForPhoneNumber(user) {
  var msg = new _TextMessage2.default("I'll need your phone number for shipping");
  var qr = new _QuickReply.QuickReply(_QuickReply.QuickReplyTypes.PHONE_NUMBER);
  msg.addQuickReply(qr);
  msg.send(user.psid);
}

function askForAddress(user) {
  var msg = new _TextMessage2.default('Perfect, now I just need the address to ship it to.');
  msg.send(user.psid);
}

exports.default = {
  askForAddress: askForAddress,
  askForName: askForName,
  askForPhoneNumber: askForPhoneNumber,
  askForQuantity: askForQuantity,
  sendResetSuccess: sendResetSuccess,
  sendEndDemo: sendEndDemo,
  sendGreetings: sendGreetings,
  sendHOPNotice: sendHOPNotice,
  sendMoreInfo: sendMoreInfo,
  sendOrderAccepted: sendOrderAccepted,
  sendOrderConfirmation: sendOrderConfirmation
};
//# sourceMappingURL=chatscript.js.map