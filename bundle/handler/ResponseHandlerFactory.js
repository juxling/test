'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _default = require('./default');

var _default2 = _interopRequireDefault(_default);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Add here new client-specific handlers - see 'default' as ex.
var handlerMap = {
  'default': _default2.default
};

var ResponseHandlerFactory = function ResponseHandlerFactory(name) {
  _classCallCheck(this, ResponseHandlerFactory);

  return new handlerMap[name]();
};

exports.default = ResponseHandlerFactory;
//# sourceMappingURL=ResponseHandlerFactory.js.map