'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BaseResponseHandler = function () {
  function BaseResponseHandler() {
    _classCallCheck(this, BaseResponseHandler);
  }

  _createClass(BaseResponseHandler, [{
    key: 'messageHandler',
    value: function messageHandler(user, message) {
      throw new Error('messageHandler() not implemented');
    }
  }, {
    key: 'postbackHandler',
    value: function postbackHandler(user, postback) {
      throw new Error('postbackHandler() not implemented');
    }
  }, {
    key: 'handoverHandler',
    value: function handoverHandler(user, handover) {
      throw new Error('handoverHandler() not implemented');
    }
  }]);

  return BaseResponseHandler;
}();

exports.default = BaseResponseHandler;
//# sourceMappingURL=BaseResponseHandler.js.map