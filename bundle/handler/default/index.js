'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _BaseResponseHandler2 = require('../BaseResponseHandler');

var _BaseResponseHandler3 = _interopRequireDefault(_BaseResponseHandler2);

var _constants = require('../../common/constants');

var _constants2 = _interopRequireDefault(_constants);

var _sales = require('../sales');

var _sales2 = _interopRequireDefault(_sales);

var _config = require('../../common/config');

var _config2 = _interopRequireDefault(_config);

var _logger = require('../../common/logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DefaultResponseHandler = function (_BaseResponseHandler) {
  _inherits(DefaultResponseHandler, _BaseResponseHandler);

  function DefaultResponseHandler() {
    _classCallCheck(this, DefaultResponseHandler);

    var _this = _possibleConstructorReturn(this, (DefaultResponseHandler.__proto__ || Object.getPrototypeOf(DefaultResponseHandler)).call(this));

    _this.handlers = [];
    var features = _config2.default.features;
    features.forEach(function (feature) {
      switch (feature) {
        case _constants2.default.FEATURES_SALES:
          _this.handlers.push(new _sales2.default().initialize());
          break;
      }
    });
    return _this;
  }

  _createClass(DefaultResponseHandler, [{
    key: 'messageHandler',
    value: function messageHandler(user, message) {
      var isHandled = false;
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.handlers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var handler = _step.value;

          if (handler.messageHandler(user, message)) {
            isHandled = true;
            break;
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      if (!isHandled) {
        // TODO: handle gracefully with a GIVE UP message
        _logger2.default.info('unexpected message received');
        this.giveUpHandler();
      }
    }
  }, {
    key: 'postbackHandler',
    value: function postbackHandler(user, postback) {
      var isHandled = false;
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = this.handlers[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var handler = _step2.value;

          if (handler.postbackHandler(user, postback)) {
            isHandled = true;
            break;
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      if (!isHandled) {
        // TODO: handle gracefully with a GIVE UP message
        _logger2.default.info('unexpected postback received');
        this.giveUpHandler();
      }
    }
  }, {
    key: 'handoverHandler',
    value: function handoverHandler(user, handover) {
      var isHandled = false;
      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = this.handlers[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var handler = _step3.value;

          if (handler.handoverHandler(user, handover)) {
            isHandled = true;
            break;
          }
        }
      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3.return) {
            _iterator3.return();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }

      if (!isHandled) {
        _logger2.default.info('unexpected handover received');
      }
    }
  }, {
    key: 'giveUpHandler',
    value: function giveUpHandler() {}
    // TODO implement this to handle errors gracefully

    // TODO add conversational behavior with sectioned text and seen/typing behavior

  }]);

  return DefaultResponseHandler;
}(_BaseResponseHandler3.default);

exports.default = DefaultResponseHandler;
//# sourceMappingURL=index.js.map