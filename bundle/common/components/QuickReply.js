'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var QuickReplyTypes = Object.freeze({
  TEXT: 'text',
  LOCATION: 'location',
  PHONE_NUMBER: 'user_phone_number',
  EMAIL: 'user_email'
});

var QuickReply = function () {
  function QuickReply(contentType) {
    _classCallCheck(this, QuickReply);

    this.contentType = contentType;
  }

  _createClass(QuickReply, [{
    key: 'setTitle',
    value: function setTitle(title) {
      this.title = title;
    }
  }, {
    key: 'setPayload',
    value: function setPayload(payload) {
      this.payload = payload;
    }
  }, {
    key: 'sanitize',
    value: function sanitize() {
      return {
        'content_type': this.contentType,
        'title': this.title,
        'payload': this.payload
      };
    }
  }]);

  return QuickReply;
}();

exports.QuickReply = QuickReply;
exports.QuickReplyTypes = QuickReplyTypes;
//# sourceMappingURL=QuickReply.js.map