'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _messageQueueHandler = require('../messageQueueHandler');

var _messageQueueHandler2 = _interopRequireDefault(_messageQueueHandler);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BaseComponent = function () {
  function BaseComponent() {
    _classCallCheck(this, BaseComponent);
  }

  _createClass(BaseComponent, [{
    key: 'send',
    value: function send(psid, payload) {
      var messageObj = {
        recipient: { id: psid },
        message: payload
      };

      return _messageQueueHandler2.default.addMessageJob(psid, messageObj);
    }
  }]);

  return BaseComponent;
}();

exports.default = BaseComponent;
//# sourceMappingURL=BaseComponent.js.map