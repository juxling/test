'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//https://developers.facebook.com/docs/messenger-platform/reference/buttons/postback

var PostbackButton = function PostbackButton(title, payload) {
  _classCallCheck(this, PostbackButton);

  this.type = 'postback';
  this.title = title;
  this.payload = payload;
};

exports.default = PostbackButton;
//# sourceMappingURL=PostBackButton.js.map