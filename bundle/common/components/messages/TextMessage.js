'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BaseComponent2 = require('../BaseComponent');

var _BaseComponent3 = _interopRequireDefault(_BaseComponent2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextMessage = function (_BaseComponent) {
  _inherits(TextMessage, _BaseComponent);

  function TextMessage(text) {
    _classCallCheck(this, TextMessage);

    var _this = _possibleConstructorReturn(this, (TextMessage.__proto__ || Object.getPrototypeOf(TextMessage)).call(this));

    _this.text = text;
    _this.quickReplies = [];
    return _this;
  }

  _createClass(TextMessage, [{
    key: 'addQuickReply',
    value: function addQuickReply(quickReply) {
      this.quickReplies.push(quickReply.sanitize());
    }
  }, {
    key: 'send',
    value: function send(psid) {
      var messagePayload = {
        'text': this.text
      };
      if (this.quickReplies.length > 0) {
        messagePayload['quick_replies'] = this.quickReplies;
      }
      return _get(TextMessage.prototype.__proto__ || Object.getPrototypeOf(TextMessage.prototype), 'send', this).call(this, psid, messagePayload);
    }
  }]);

  return TextMessage;
}(_BaseComponent3.default);

exports.default = TextMessage;
//# sourceMappingURL=TextMessage.js.map