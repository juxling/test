'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TemplateTypes = exports.BaseTemplate = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BaseComponent2 = require('../BaseComponent');

var _BaseComponent3 = _interopRequireDefault(_BaseComponent2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TemplateTypes = Object.freeze({
  GENERIC: 'generic',
  BUTTON: 'button',
  RECEIPT: 'receipt'
});

var BaseTemplate = function (_BaseComponent) {
  _inherits(BaseTemplate, _BaseComponent);

  function BaseTemplate() {
    _classCallCheck(this, BaseTemplate);

    return _possibleConstructorReturn(this, (BaseTemplate.__proto__ || Object.getPrototypeOf(BaseTemplate)).apply(this, arguments));
  }

  _createClass(BaseTemplate, [{
    key: 'send',
    value: function send(psid, templatePayload) {
      var messagePayload = {
        'attachment': {
          'type': 'template',
          'payload': templatePayload
        }
      };
      return _get(BaseTemplate.prototype.__proto__ || Object.getPrototypeOf(BaseTemplate.prototype), 'send', this).call(this, psid, messagePayload);
    }
  }]);

  return BaseTemplate;
}(_BaseComponent3.default);

exports.BaseTemplate = BaseTemplate;
exports.TemplateTypes = TemplateTypes;
//# sourceMappingURL=BaseTemplate.js.map