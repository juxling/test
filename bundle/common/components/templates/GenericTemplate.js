'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BaseTemplate2 = require('./BaseTemplate');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// GenericTemplate supports carousel, but we will omit for now
// https://developers.facebook.com/docs/messenger-platform/reference/template/generic/
var GenericTemplate = function (_BaseTemplate) {
  _inherits(GenericTemplate, _BaseTemplate);

  function GenericTemplate() {
    _classCallCheck(this, GenericTemplate);

    var _this = _possibleConstructorReturn(this, (GenericTemplate.__proto__ || Object.getPrototypeOf(GenericTemplate)).call(this));

    _this.type = _BaseTemplate2.TemplateTypes.GENERIC;
    _this.buttons = [];
    return _this;
  }

  _createClass(GenericTemplate, [{
    key: 'setTitle',
    value: function setTitle(title) {
      this.title = title;
    }
  }, {
    key: 'setSubtitle',
    value: function setSubtitle(subtitle) {
      this.subtitle = subtitle;
    }
  }, {
    key: 'setImageUrl',
    value: function setImageUrl(url) {
      this.image_url = url;
    }
  }, {
    key: 'addButton',
    value: function addButton(button) {
      this.buttons.push(button);
    }
  }, {
    key: 'send',
    value: function send(psid) {
      var templatePayload = {
        'template_type': this.type,
        'elements': [{
          'title': this.title,
          'image_url': this.image_url,
          'subtitle': this.subtitle,
          'buttons': this.buttons
        }]
      };
      _get(GenericTemplate.prototype.__proto__ || Object.getPrototypeOf(GenericTemplate.prototype), 'send', this).call(this, psid, templatePayload);
    }
  }]);

  return GenericTemplate;
}(_BaseTemplate2.BaseTemplate);

exports.default = GenericTemplate;
//# sourceMappingURL=GenericTemplate.js.map