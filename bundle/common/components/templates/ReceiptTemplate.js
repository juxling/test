'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _BaseTemplate2 = require('./BaseTemplate');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// https://developers.facebook.com/docs/messenger-platform/reference/template/receipt/
var ReceiptTemplate = function (_BaseTemplate) {
  _inherits(ReceiptTemplate, _BaseTemplate);

  function ReceiptTemplate() {
    _classCallCheck(this, ReceiptTemplate);

    var _this = _possibleConstructorReturn(this, (ReceiptTemplate.__proto__ || Object.getPrototypeOf(ReceiptTemplate)).call(this));

    _this.type = _BaseTemplate2.TemplateTypes.RECEIPT;
    return _this;
  }

  _createClass(ReceiptTemplate, [{
    key: 'setRecipientName',
    value: function setRecipientName(name) {
      this.recipientName = name;
    }
  }, {
    key: 'setOrderNumber',
    value: function setOrderNumber(number) {
      this.orderNumber = number;
    }
  }, {
    key: 'setCurrency',
    value: function setCurrency(currency) {
      this.currency = currency;
    }
  }, {
    key: 'setPaymentMethod',
    value: function setPaymentMethod(paymentMethod) {
      this.paymentMethod = paymentMethod;
    }
  }, {
    key: 'setTotalCost',
    value: function setTotalCost(cost) {
      this.totalCost = cost;
    }
  }, {
    key: 'setProductTitle',
    value: function setProductTitle(title) {
      this.productTitle = title;
    }
  }, {
    key: 'setProductDescription',
    value: function setProductDescription(description) {
      this.productDescription = description;
    }
  }, {
    key: 'setProductQuantity',
    value: function setProductQuantity(quantity) {
      this.productQuantity = quantity;
    }
  }, {
    key: 'setProductImage',
    value: function setProductImage(image) {
      this.productImage = image;
    }
  }, {
    key: 'send',
    value: function send(psid) {
      var templatePayload = {
        'template_type': this.type,
        'recipient_name': this.recipientName,
        'order_number': this.orderNumber,
        'currency': this.currency,
        'payment_method': this.paymentMethod,
        'summary': {
          'total_cost': this.totalCost
        },
        'elements': [{
          'title': this.productTitle,
          'subtitle': this.productDescription,
          'quantity': this.productQuantity,
          'price': this.totalCost,
          'currency': this.currency,
          'image_url': this.productImage
        }]
      };
      _get(ReceiptTemplate.prototype.__proto__ || Object.getPrototypeOf(ReceiptTemplate.prototype), 'send', this).call(this, psid, templatePayload);
    }
  }]);

  return ReceiptTemplate;
}(_BaseTemplate2.BaseTemplate);

exports.default = ReceiptTemplate;
//# sourceMappingURL=ReceiptTemplate.js.map