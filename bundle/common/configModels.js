'use strict';

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

// key has to be same as FEATURES_SALES
var configModels = Object.freeze({
  sales: {
    User: './sales/user',
    Order: './sales/order',
    Product: './sales/product'
  }
});

module.exports = configModels;
//# sourceMappingURL=configModels.js.map