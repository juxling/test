'use strict';

var _constants = require('./constants');

var _constants2 = _interopRequireDefault(_constants);

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

var features = [];
try {
  var features_json = JSON.parse(process.env.USE_FEATURES);
  features_json.forEach(function (feature) {
    switch (feature) {
      case _constants2.default.FEATURES_SALES:
        features.push(_constants2.default.FEATURES_SALES);
        break;
    }
  });
} catch (e) {
  _logger2.default.error('USE_FEATURES is an invalid JSON string.');
}

if (features.length == 0) {
  _logger2.default.error('No valid features found.');
}

var config = Object.freeze({
  db: {
    HOST: process.env.SB_DB_HOST,
    NAME: process.env.SB_DB_NAME,
    USERNAME: process.env.SB_DB_USER_NAME,
    USE_DB: process.env.USE_DB,
    SQLITE_PATH: process.env.SQLITE_PATH
  },
  server: {
    PORT: process.env.SB_SERVER_PORT
  },
  advertiser: {
    NAME: process.env.SB_ADVERTISER_NAME
  },
  fb: {
    APP_ID: process.env.APP_ID,
    PAGE_ACCESS_TOKEN: process.env.PAGE_ACCESS_TOKEN
  },
  features: features
});

module.exports = config;
//# sourceMappingURL=config.js.map