'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

var _configModels = require('../configModels');

var _configModels2 = _interopRequireDefault(_configModels);

var _logger = require('../logger');

var _logger2 = _interopRequireDefault(_logger);

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Database = function () {
  function Database() {
    _classCallCheck(this, Database);

    this.initdb();
  }

  _createClass(Database, [{
    key: 'initdb',
    value: function initdb() {
      var _this = this;

      var db_url = void 0;
      if (process.env.NODE_ENV === 'production') {
        db_url = process.env.DATABASE_URL;
      } else {
        if (_config2.default.db.USE_DB == 'SQLITE') {
          db_url = 'database';
        } else {
          db_url = 'postgres://' + _config2.default.db.USERNAME + '@' + _config2.default.db.HOST + '/' + _config2.default.db.NAME;
        }
      }

      var dbConfig = {
        operatorsAliases: false,
        pool: {
          max: 5,
          min: 0,
          acquire: 30000,
          idle: 10000
        },
        logging: process.env.NODE_DEV !== 'production' ? console.log : false
      };

      var sequelize = void 0;
      if (_config2.default.db.USE_DB == 'SQLITE') {
        dbConfig['dialect'] = 'sqlite';
        dbConfig['host'] = '0.0.0.0';
        dbConfig['storage'] = '' + _config2.default.db.SQLITE_PATH;
        sequelize = new _sequelize2.default('database', '', '', dbConfig); // USERNAME, PASS
      } else {
        dbConfig['dialect'] = 'postgres';
        sequelize = new _sequelize2.default(db_url, dbConfig);
      }

      this.db = {};
      sequelize.authenticate().then(function () {
        _logger2.default.info('DB connection has been established successfully.');
        _config2.default.features.forEach(function (feature) {
          for (var component_key in _configModels2.default[feature]) {
            _logger2.default.info('Adding: ' + component_key);
            _this.db[component_key] = sequelize.import(_configModels2.default[feature][component_key]);
          }
        });

        Object.keys(_this.db).forEach(function (modelName) {
          if (_this.db[modelName].associate) {
            _this.db[modelName].associate(_this.db);
          }
        });
        sequelize.sync();
      }).catch(function (err) {
        _logger2.default.error('Unable to connect to the database:', err);
      });

      this.db.sequelize = sequelize;
      this.db.Sequelize = _sequelize2.default;
    }
  }, {
    key: 'getDB',
    value: function getDB() {
      if (this.db === null) {
        this.initdb();
      }
      return this.db;
    }
  }]);

  return Database;
}();

module.exports = Database;
//# sourceMappingURL=index.js.map