'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var product = function product(sequelize, DataTypes) {
  var Product = sequelize.define('product', {
    pid: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING
    },
    images: {
      // JSON String Array because sqlite doesn't support array
      type: DataTypes.JSON,
      allowNull: false
    },
    price: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    currency: {
      type: DataTypes.STRING,
      allowNull: false
    },
    paymentMethod: {
      type: DataTypes.STRING,
      allowNull: false
    },
    hasStock: {
      type: DataTypes.BOOLEAN
    }
  });

  return Product;
};

exports.default = product;
//# sourceMappingURL=product.js.map