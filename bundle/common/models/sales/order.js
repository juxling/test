'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var order = function order(sequelize, DataTypes) {
  var Order = sequelize.define('order', {
    oid: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    pid: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    quantity: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    name: {
      type: DataTypes.STRING
    },
    address: {
      type: DataTypes.STRING
    },
    phoneNumber: {
      type: DataTypes.STRING
    }
  });

  Order.associate = function (models) {
    Order.belongsTo(models.User);
  };

  return Order;
};

exports.default = order;
//# sourceMappingURL=order.js.map