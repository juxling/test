'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var user = function user(sequelize, DataTypes) {
  var User = sequelize.define('user', {
    psid: {
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true
    },
    firstName: {
      type: DataTypes.STRING
    },
    lastName: {
      type: DataTypes.STRING
    },
    gender: {
      type: DataTypes.STRING
    },
    state: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    currentOrderId: {
      type: DataTypes.INTEGER,
      defaultValue: null
    },
    threadControl: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {
    getterMethods: {
      fullName: function fullName() {
        return this.firstName + ' ' + this.lastName;
      }
    }
  });

  User.associate = function (models) {
    User.hasMany(models.Order);
  };

  return User;
};

exports.default = user;
//# sourceMappingURL=user.js.map