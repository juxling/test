'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _constants = require('./constants');

var _constants2 = _interopRequireDefault(_constants);

var _fbrequest = require('./fbrequest');

var _fbrequest2 = _interopRequireDefault(_fbrequest);

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function pass_thread_control(psid) {
  var hopPayload = {
    'recipient': { 'id': psid },
    'target_app_id': _constants2.default.PAGES_INBOX_APP_ID,
    'metadata': 'test metadata from bot'
  };

  var page_access_token = _config2.default.fb.PAGE_ACCESS_TOKEN;
  _logger2.default.info('Initiating handover to Pages Inbox');

  return _fbrequest2.default.post({
    uri: _constants2.default.GRAPH_BASE_URL + '/me/pass_thread_control',
    qs: { access_token: page_access_token },
    json: hopPayload
  }).then(function () {
    _logger2.default.info('Handover to Pages Inbox success');
  }).catch(function (err) {
    _logger2.default.info('Handover to Pages Inbox failed: ' + JSON.stringify(err));
  });
}

function take_thread_control(psid) {
  var hopPayload = {
    'recipient': { 'id': psid }
  };

  var page_access_token = _config2.default.fb.PAGE_ACCESS_TOKEN;
  _logger2.default.info('Initiating takeover from Pages Inbox');

  return _fbrequest2.default.post({
    uri: _constants2.default.GRAPH_BASE_URL + '/me/take_thread_control',
    qs: { access_token: page_access_token },
    json: hopPayload
  }).then(function () {
    _logger2.default.info('Takeover from Pages Inbox success');
  }).catch(function (err) {
    _logger2.default.info('Takeover from Pages Inbox failed: ' + JSON.stringify(err));
  });
}

exports.default = {
  pass_thread_control: pass_thread_control,
  take_thread_control: take_thread_control
};
//# sourceMappingURL=hop.js.map