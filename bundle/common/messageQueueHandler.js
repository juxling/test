'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _messagequeue = require('./messagequeue');

var _messagequeue2 = _interopRequireDefault(_messagequeue);

var _messagequeueSQLite = require('./messagequeueSQLite');

var _messagequeueSQLite2 = _interopRequireDefault(_messagequeueSQLite);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var messageQueueHandler = void 0;
if (_config2.default.db.USE_DB == 'SQLITE') {
  messageQueueHandler = _messagequeueSQLite2.default;
} else {
  messageQueueHandler = _messagequeue2.default;
}
exports.default = {
  addMessageJob: function addMessageJob(psid, messageObj) {
    return messageQueueHandler.addMessageJob(psid, messageObj);
  }
};
//# sourceMappingURL=messageQueueHandler.js.map