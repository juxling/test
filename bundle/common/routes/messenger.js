'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

var _constants = require('../constants');

var _constants2 = _interopRequireDefault(_constants);

var _models = require('../models');

var _models2 = _interopRequireDefault(_models);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _fbrequest = require('../fbrequest');

var _fbrequest2 = _interopRequireDefault(_fbrequest);

var _logger = require('../logger');

var _logger2 = _interopRequireDefault(_logger);

var _ResponseHandlerFactory = require('../../handler/ResponseHandlerFactory');

var _ResponseHandlerFactory2 = _interopRequireDefault(_ResponseHandlerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

//Messenger Webhook Verification
router.get('/webhook', function (req, res) {
  var mode = req.query['hub.mode'];
  var token = req.query['hub.verify_token'];
  var challenge = req.query['hub.challenge'];

  if (mode && token) {
    if (mode === 'subscribe' && token === _constants2.default.WEBHOOK_VERIFY_TOKEN) {
      _logger2.default.info('Webhook Verification Complete');
      res.status(200).send(challenge);
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});

//Messenger Webhook Events
router.post('/webhook', function (req, res) {
  res.sendStatus(200); //respond immediately

  var body = req.body;
  if (body.object === 'page') {
    body.entry.forEach(function (entry) {
      if (entry.messaging) {
        entry.messaging.forEach(function (messagingEvent) {
          _logger2.default.info('incoming event:\n' + JSON.stringify(messagingEvent, null, 2));

          _models2.default.User.findOrCreate({
            where: { psid: messagingEvent.sender.id }
          }).spread(function (user, created) {
            if (created) {
              return updateUserProfile(user);
            }
            return user;
          }).then(function (user) {
            var handler = new _ResponseHandlerFactory2.default(_config2.default.advertiser.NAME);
            handler.then(function () {
              if (messagingEvent.message) {
                handler.messageHandler(user, messagingEvent.message);
              } else if (messagingEvent.postback) {
                handler.postbackHandler(user, messagingEvent.postback);
              } else if (messagingEvent.pass_thread_control) {
                handler.handoverHandler(user, messagingEvent.pass_thread_control);
              } else {
                _logger2.default.info('unexpected event');
              }
            });
          });
        });
      } else if (entry.standby) {
        entry.standby.forEach(function (messagingEvent) {
          //do nothing for standby
          _logger2.default.info('GOT STANDBY: ' + JSON.stringify(messagingEvent, null, 2));
        });
      } else {
        _logger2.default.info('unexpected entry');
      }
    });
  }
});

var updateUserProfile = function updateUserProfile(user) {
  _logger2.default.info('Getting user profile for psid: ' + user.psid);
  return _fbrequest2.default.get({
    uri: _constants2.default.GRAPH_BASE_URL + '/' + user.psid,
    qs: {
      fields: 'first_name, last_name',
      access_token: _config2.default.fb.PAGE_ACCESS_TOKEN
    }
  }).then(function (res) {
    _logger2.default.info('Received user profile, updating DB');
    return user.update({
      firstName: res.first_name,
      lastName: res.last_name
    });
  }).catch(function (err) {
    return _logger2.default.info('Could not retreive user profile:\n' + JSON.stringify(err, null, 2));
  }).then(function () {
    return user;
  });
};

exports.default = router;
//# sourceMappingURL=messenger.js.map