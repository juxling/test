'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _nodePersistentQueue = require('node-persistent-queue');

var _nodePersistentQueue2 = _interopRequireDefault(_nodePersistentQueue);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _constants = require('./constants');

var _constants2 = _interopRequireDefault(_constants);

var _fbrequest = require('./fbrequest');

var _fbrequest2 = _interopRequireDefault(_fbrequest);

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var q = new _nodePersistentQueue2.default('' + _config2.default.db.SQLITE_PATH, 1);

q.on('open', function () {
  _logger2.default.info('Opening SQLite DB');
  _logger2.default.info('Queue contains ' + q.getLength() + ' job/s');
});

q.on('add', function (task) {
  _logger2.default.info('Adding task: ' + JSON.stringify(task));
  _logger2.default.info('Queue contains ' + q.getLength() + ' jobs');
});

q.on('start', function () {
  _logger2.default.info('Starting queue');
});

q.on('next', function (task) {
  messageJobHandler(task);
});

q.on('empty', function () {
  _logger2.default.info('Queue is empty');
});

q.on('stop', function () {
  _logger2.default.info('Stopping queue');
});

q.on('close', function () {
  _logger2.default.info('Closing SQLite DB');
});

q.open().then(function () {
  q.start();
}).catch(function (err) {
  onError(err);
});

function onError(error) {
  _logger2.default.info('message-job error:' + JSON.stringify(error, null, 2));
}

/* worker function */
function messageJobHandler(job) {
  var messageObj = job.messageObj;
  _logger2.default.info('Sending message ' + JSON.stringify(messageObj) + '.');
  _fbrequest2.default.post({
    uri: _constants2.default.GRAPH_BASE_URL + '/me/messages',
    qs: { access_token: _config2.default.fb.PAGE_ACCESS_TOKEN },
    json: messageObj
  }).then(_logger2.default.info('Message sent successfully')).catch(function (err) {
    return _logger2.default.info('Message sending failed with error: ' + err);
  }).finally(function () {
    q.done();
  });
}

exports.default = {
  addMessageJob: function addMessageJob(psid, messageObj) {
    return q.add({ psid: psid, messageObj: messageObj });
  }
};
//# sourceMappingURL=messagequeueSQLite.js.map