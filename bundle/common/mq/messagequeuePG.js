'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pgBoss = require('pg-boss');

var _pgBoss2 = _interopRequireDefault(_pgBoss);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _constants = require('./constants');

var _constants2 = _interopRequireDefault(_constants);

var _fbrequest = require('./fbrequest');

var _fbrequest2 = _interopRequireDefault(_fbrequest);

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var db_url = void 0;
if (process.env.NODE_ENV === 'production') {
  db_url = process.env.DATABASE_URL;
} else {
  db_url = 'postgres://' + _config2.default.db.USERNAME + '@' + _config2.default.db.HOST + '/' + _config2.default.db.NAME;
}

var queueName = 'message-job';
var options = {
  connectionString: db_url,
  //TODO: to make sure for the same destination psid, jobs are processed in order, we need concurrency control
  //setting global pool size to 1, but in the future we need to customize subscribe to only limit to psid
  //https://github.com/timgit/pg-boss/issues/63
  poolSize: 1,
  archiveCompletedJobsEvery: '2 days'
};

// TODO make this into a class
var boss = void 0;
if (_config2.default.db.USE_DB === 'POSTGRES') {
  boss = new _pgBoss2.default(options);
  boss.on('error', onError);
  boss.start().then(ready).catch(onError);
}

function ready() {
  boss.subscribe(queueName, messageJobHandler).then(_logger2.default.info('Subscribed to ' + queueName + ' queue')).catch(onError);

  _logger2.default.info(queueName + ' queue ready');
}

function onError(error) {
  _logger2.default.info('message-job error:' + JSON.stringify(error, null, 2));
}

/* worker function */
function messageJobHandler(job) {
  var messageObj = job.data.messageObj;
  _logger2.default.info('Processing job id:' + job.id + ')');
  _logger2.default.info('Sending message ' + JSON.stringify(messageObj) + '.');
  _fbrequest2.default.post({
    uri: _constants2.default.GRAPH_BASE_URL + '/me/messages',
    qs: { access_token: _config2.default.fb.PAGE_ACCESS_TOKEN },
    json: messageObj
  }).then(_logger2.default.info('Message sent successfully')).catch(function (err) {
    return _logger2.default.info('Message sending failed with error: ' + err);
  }).finally(function () {
    job.done().then(_logger2.default.info('Job ' + job.id + ' completed')).catch(onError);
  });
}

exports.default = {
  addMessageJob: function addMessageJob(psid, messageObj) {
    return boss.publish(queueName, { psid: psid, messageObj: messageObj }).then(function (jobId) {
      _logger2.default.info('Message \n' + JSON.stringify(messageObj, null, 2) + ' added to job queue, id:' + jobId);
      return jobId;
    }).catch(onError);
  }
};
//# sourceMappingURL=messagequeuePG.js.map