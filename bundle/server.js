'use strict';

var _config = require('./common/config');

var _config2 = _interopRequireDefault(_config);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _logger = require('./common/logger');

var _logger2 = _interopRequireDefault(_logger);

var _routes = require('./common/routes');

var _routes2 = _interopRequireDefault(_routes);

var _models = require('./common/models');

var _models2 = _interopRequireDefault(_models);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use('/', _routes2.default);

// force: false means do not drop tables if exist
var db = new _models2.default();
db.getDB().sequelize.sync({ force: false }).then(function () {
  app.listen(process.env.PORT || _config2.default.server.PORT, function () {
    _logger2.default.info('Salesbot listening on port ' + _config2.default.server.PORT + ' with\nconfig = ' + JSON.stringify(_config2.default, null, 2));
  });
});

module.exports = db;

//# sourceMappingURL=server.js.map